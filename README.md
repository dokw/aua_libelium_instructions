# Инструкции для проверки сенсоров Liblelium

### Сборка
#### Вставляем сенсоры в нужные порты (очень важно не ошибиться с гнездом!)
сенсор | гнездо
--- | ---
*NO2* | `A`
*SO2* | `B`
*CO* | `C`
*NO* | `F`
*Temperature, humidity and pressure* | `E`


![Screen-shot](https://i.postimg.cc/Dy0F0Zc0/IMG-DDD2-C2-BABAA5-1.jpg)

#### 1. Скачиваем среду для разработки => [Waspmote IDE](http://www.libelium.com/development/waspmote/sdk_applications/)

#### 3. Открываем файл `test_metrics.pde` в Waspmote IDE

#### 2. Включаем Libelium(черная кнопка с боку, после нажатия должна мигнуть красным) и подключаем его к USB порту на компьютере 

#### 4. Выбираем нужный порт на Waspmote IDE 
##### *(обычно это последний порт в списке, но если не заработает попробуйте другие)*
<br>
![Screen-Shot.png](https://i.postimg.cc/nr208pWQ/Screen-Shot.png)

#### 3. Загружаем код и смотрим в `Serial Monitor`

![Screen-Shot-201.png](https://i.postimg.cc/SQYgDbVR/Screen-Shot-201.png)

#### 5. `baud` rate нужно обезательно поставить на 115200
![](https://i.postimg.cc/yxKnDkRG/Screen-Shot-2018-11.png)

#### 6. Ждем примерно 10 минут пока данные не нормализуются (сенсоры должны "разогреться")



