/// NOTES:
/// * Change deep sleep time
/// * Test with sensors connected


// Put your libraries here (#include ...)
#include <WaspWIFI_PRO.h>
#include <WaspSensorGas_Pro.h>
#include <WaspFrame.h>


// Sensors sockets
///////////////////////////////////////
Gas NO2(SOCKET_A); 
Gas SO2(SOCKET_B);
Gas CO(SOCKET_C);
Gas NO(SOCKET_F);
Gas TempHP(SOCKET_E);
///////////////////////////////////////

// Sensor variables
///////////////////////////////////////
float temperature;
float humidity;
float pressure;

float NOconc;
float NO2conc;
float COconc;
float SO2conc;
///////////////////////////////////////


// example of body for POST request

uint8_t error;
uint8_t status;
unsigned long previous;

void send_data(
  float temperature,
  float humidity,
  float pressure,
  float NO2conc,
  float NOconc,
  float COconc,
  float SO2conc
);


void setup()
{
  USB.println("====================== SETUP ======================");
  NO2.ON();
  NO.ON();
  CO.ON();
  SO2.ON();
  TempHP.ON();
  PWR.deepSleep("00:00:00:30", RTC_OFFSET, RTC_ALM1_MODE1, ALL_ON);
}



void loop() {
  // Read the sensors and compensate with the temperature internally


  NO2conc = NO2.getConc();
  NOconc = NO.getConc();
  COconc = CO.getConc();
  SO2conc = SO2.getConc();
  // Read enviromental variables
  temperature = TempHP.getTemp();
  humidity = TempHP.getHumidity();
  pressure = TempHP.getPressure();

  ///////////////////////////////////////////
  // 4. Sleep
  ///////////////////////////////////////////
  float gas_values[] = {
    temperature,
    humidity,
    pressure,
    COconc,
    NO2conc,
    NOconc,
    SO2conc
  };

  char* gas_names[] = {"temperature", "humidity", "pressure", "CO", "NO2", "NO", "SO2"};

  for (int i = 0; i < 7; i++) {
    USB.print(gas_names[i]);
    USB.print(": ");
    USB.println(gas_values[i]);
  }

  USB.println("======================================================");
  // Go to deepsleep
  PWR.deepSleep("00:00:00:15", RTC_OFFSET, RTC_ALM1_MODE1, ALL_OFF);
}



